package tests

import (
	"creational_patterns/abstract_factory"
	"creational_patterns/builder"
	"creational_patterns/singleton"
	"reflect"
	"testing"
)

// AssertEqual checks if values are equal
func AssertEqual(t *testing.T, given interface{}, expected interface{}) {
	if reflect.DeepEqual(given, expected) { return }
	t.Errorf("Received %v (type %v), expected %v (type %v)", given, reflect.TypeOf(given), expected, reflect.TypeOf(expected))
}

func TestBuildArmy_ShouldBeOf25Soldiers(t *testing.T) {
	// System start
	singleton.GetInstance().SetValue("warriorFactory", &abstract_factory.DemonWarriorFactory{})

	// Later in another module
	dependency, _ := singleton.GetInstance().Get("warriorFactory") // Locate dependency
	warriorFactory := dependency.(abstract_factory.WarriorFactory)    // Cast

	armyOfClones, _ := builder.
		NewArmyBuilder(warriorFactory).
		Evil().
		Count(25).
		Build()

	AssertEqual(t, len(armyOfClones.Soldiers), 25)
}