package builder

import (
	"creational_patterns/abstract_factory"
	"creational_patterns/factory_method"
	"creational_patterns/model"
)

type Army struct {
	Soldiers []model.Soldier
}

// Fluent Builder (Builder methods return Builder)

type Builder interface {
	Count(int) Builder
	Evil() Builder
	Human() Builder
	Build() (*Army, bool)
}

type ArmyBuilder struct {
	factory abstract_factory.WarriorFactory
	isEvil bool
	count int
}

func NewArmyBuilder(factory abstract_factory.WarriorFactory) Builder {
	return &ArmyBuilder{factory: factory, isEvil: false, count: 1}
}

// Fluent implementation

func (ab *ArmyBuilder) Count(count int) Builder {
	ab.count = count
	return ab
}

func (ab *ArmyBuilder) Evil() Builder {
	ab.isEvil = true
	return ab
}

func (ab *ArmyBuilder) Human() Builder {
	ab.isEvil = false
	return ab
}

func (ab *ArmyBuilder) Build() (*Army, bool) {
	if ab.count < 1 || ab.count > 10000 { return nil, false }

	soldier := MakeSoldiersFromBlueprint(ab.factory, ab.isEvil)
	soldiers := CloneSoldier(soldier, ab.count)

	return &Army{ Soldiers: soldiers }, true
}

// Utility Methods

func CloneSoldier(soldier model.Soldier, times int) []model.Soldier {
	var soldiers []model.Soldier
	for i := 0; i < times; i++ {
		soldiers = append(soldiers, soldier.Clone())
	}
	return soldiers
}

func MakeSoldiersFromBlueprint(factory abstract_factory.WarriorFactory, isEvil bool) model.Soldier {
	if isEvil {
		return factory_method.MakeEvilSoldier(factory)
	}

	return factory_method.MakeDefaultSoldier(factory)
}