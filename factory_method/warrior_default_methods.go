package factory_method

import (
	"creational_patterns/abstract_factory"
	"creational_patterns/model"
)

func MakeDefaultSoldier(factory abstract_factory.WarriorFactory) model.Soldier {
	preferredWeapon := factory.MakePreferredWeapon()
	preferredShield := factory.MakePreferredShield()
	return factory.MakeSoldier(preferredWeapon, preferredShield)
}

func MakeEvilSoldier(factory abstract_factory.WarriorFactory) model.Soldier {
	preferredWeapon := factory.MakeEvilWeapon()
	preferredShield := &model.EmptyShield{}
	return factory.MakeSoldier(preferredWeapon, preferredShield)
}

