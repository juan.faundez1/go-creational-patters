package singleton

import "sync"

var lock = &sync.Mutex{}

var instance Container

func GetInstance() Container {
	if instance == nil {
		lock.Lock()
		defer lock.Unlock()
		if instance == nil {
			instance = NewContainer()
		}
	}

	return instance
}