package abstract_factory

import "creational_patterns/model"

type WarriorFactory interface {
	MakePreferredWeapon() model.Weapon
	MakeEvilWeapon() model.Weapon
	MakePreferredShield() model.Shield
	MakeSoldier(weapon model.Weapon, shield model.Shield) model.Soldier

	// Default Implementation. Factory Methods
	// MakeDefaultSoldier() Soldier
	// MakeEvilSoldier() Soldier
}

