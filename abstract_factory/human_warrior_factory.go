package abstract_factory

import (
	"creational_patterns/model"
)

type HumanWarriorFactory struct { }

func (hwf *HumanWarriorFactory) MakePreferredWeapon() model.Weapon {
	return &model.SwordWeapon{}
}
func (hwf *HumanWarriorFactory) MakeEvilWeapon() model.Weapon {
	return &model.WipWeapon{}
}
func (hwf *HumanWarriorFactory) MakePreferredShield() model.Shield {
	return &model.AdamantiumShield{}
}
func (hwf *HumanWarriorFactory) MakeSoldier(weapon model.Weapon, shield model.Shield) model.Soldier {
	return &model.HumanSoldier{ Weapon: weapon, Shield: shield }
}

/*
func (dwf *HumanWarriorFactory) MakeDefaultSoldier() contract.Soldier {
	return &model.HumanSoldier{
		Weapon: dwf.MakeWeapon(),
		Shield: &model.EmptyShield{},
	}
}*/
