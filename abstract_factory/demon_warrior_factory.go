package abstract_factory

import (
	"creational_patterns/model"
)

type DemonWarriorFactory struct { }

func (dwf *DemonWarriorFactory) MakePreferredWeapon() model.Weapon {
	return &model.SwordWeapon{}
}
func (dwf *DemonWarriorFactory) MakeEvilWeapon() model.Weapon {
	return &model.EvilWeapon{}
}
func (dwf *DemonWarriorFactory) MakePreferredShield() model.Shield {
	return &model.AdamantiumShield{}
}
func (dwf *DemonWarriorFactory) MakeSoldier(weapon model.Weapon, shield model.Shield) model.Soldier {
	return &model.DemonSoldier{ Weapon: weapon, Shield: shield }
}

/*
func (dwf *DemonWarriorFactory) MakeDefaultSoldier() contract.Soldier {
	return &model.DemonSoldier{
		Weapon: dwf.MakePreferredWeapon(),
		Shield: dwf.MakePreferredShield(),
	}
}*/

