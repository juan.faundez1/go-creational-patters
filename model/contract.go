package model

type Weapon interface { Damage() int }
type Shield interface { Defense() int }

type Soldier interface {
	Damage() int
	Defense() int
	Kills() int
	Clone() Soldier
}
