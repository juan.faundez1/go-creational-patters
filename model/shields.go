package model

type AdamantiumShield struct { }
func (as *AdamantiumShield) Defense() int { return 100 }

type EmptyShield struct { }
func (es *EmptyShield) Defense() int { return 0 }

