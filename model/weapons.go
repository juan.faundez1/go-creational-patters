package model

type SwordWeapon struct { }
func (sp *SwordWeapon) Damage() int { return 100 }

type WipWeapon struct { }
func (sp *WipWeapon) Damage() int { return 999 }

type EmptyWeapon struct { }
func (ew *EmptyWeapon) Damage() int { return 0 }

type EvilWeapon struct { }
func (sp *EvilWeapon) Damage() int { return 666 }