package model

type HumanSoldier struct {
	Weapon Weapon
	Shield Shield
	kills  int
}

func (hs *HumanSoldier) Kills() int   { return hs.kills }
func (hs *HumanSoldier) Damage() int  { return hs.Weapon.Damage() }
func (hs *HumanSoldier) Defense() int { return hs.Shield.Defense() }

type DemonSoldier struct {
	Weapon Weapon
	Shield Shield
	kills int
}

func (ds *DemonSoldier) Kills() int   { return ds.kills }
func (hs *DemonSoldier) Damage() int  { return hs.Weapon.Damage() }
func (hs *DemonSoldier) Defense() int { return hs.Shield.Defense() }

func (hs *HumanSoldier) Clone() Soldier {
	return &HumanSoldier{
		Weapon: hs.Weapon,
		Shield: hs.Shield,
		kills: 0,
	}
}

func (ds *DemonSoldier) Clone() Soldier {
	return &DemonSoldier{
		Weapon: ds.Weapon,
		Shield: ds.Shield,
		kills: 0,
	}
}

